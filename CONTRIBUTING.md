# Contributing

If you wanted to make any contributions to the project, I'd be glad to receive
them! If you find any bugs or things that aren't really good, just open up an
issue or send me a PR. And, if you have any ideas or suggestions, feel free to
send them to me :D.

Please do keep in mind that the program is meant to never exceed 200 SLOC.
