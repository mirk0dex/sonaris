#!/usr/bin/env bash

#    Copyright (C) 2021-2022 mirk0dex
#    <mirkodi.tech>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

# SONARiS Bash: a script that lets you listen to tha SoundCloud from your CLI.
# NOTE: This is the DEPRECATED Bash version, which will no longer be updated,
# use ./sonaris.sh instead (POSIX version).
#
# Deps:
# bash, GNU sed, grep (replaceable with rg), some fuzzy finder (default
# being fzf), curl, a music player (default is mpv) and youtube-dl


## SETTINGS ######################################################################################

# folders
downloadDir="$HOME/Music/SoundCloud-downloads" # default is/was $HOME/Music/SoundCloud-

# default stuffs
player="mpv" # default is mpv
finder="fzf" # default is fzf

favArtists() {
# under the following non-comment line (echo "), you may add as many artists as
# you want. You should use the name which you can see in the artist's page,
# example: if my fav artist's page URL were 'https://soundcloud.com/s3rl', I'd
# add 's3rl' to the list below
    echo "s3rl
thefatrat
foobar"
}

##################################################################################################


## MAIN VARIABLES ################################################################################

# this is the current version of the program
version="Stable (Deprecated) 1.0.2 - Codename sus"

# define a couple coloUrs
YELLOW='\033[1;33m'
RED='\033[0;31m'
NC='\033[0m' # no colour

##################################################################################################


# print help screen if needed/requested
helpMe() {
	printf "
SONARiS - DEPRECATED CLI SoundCloud client and downloader [Version %s]

Usage:\t%s
\t%s [options] [option stuffs...]

SONARiS is a script which allows you to listen to your favourite songs, albums
made by the artists you love. It does so using the amazing MPV and youtube-dl
programs and some other nice tools.

To make it your own and configure it, just open up 'sonaris.sh' in your
favoUrite text editor :D.

This is free software licensed under the GPL 3.0 license. If you didn't receive
a copy of it along with this program, check out <https://www.gnu.org/licenses/>.

Options:
  -h, --help\t\tprints this help screen and exits
  -s, --search [query]\tsearch on SoundCloud
  -d, --download\tdownload a song instead of listening to it
  -f, --favs\t\tpick an artist from a list (you can choose which to show by
\t\t\tediting the script, trust me, it's ez af)
  -v, --version\t\tprints program version and exits

[Author: mirk0dex <mirkodi.tech>]\n
" "$version" "$0" "$0"
}


## FLAGS #######################################################################################################

case $1 in
  -h|--help)      helpMe
                  exit;;
  -s|--search)    if [ -z "$2" ]; then
                    echo "${RED}Error${NC}: please specify what to search for.\nSee ${YELLOW}--help${NC} for more info."
                    exit 100
                  else
                    query="$2"
                  fi;;
  -d|--download)  player="youtube-dl";;
  -f|--favs)      choice=$(favArtists | $finder)
                  if [ -z "$choice" ]; then echo "Goodbye!"; exit; fi # if no choice then exit
                  $player "https://soundcloud.com/$choice"
                  echo "Goodbye!" && exit;;
  -v|--version)   echo "The program is currently in version:"; echo "$version"
                  exit
esac

################################################################################################################


# if no query is specified using -s or --search, ask for one
enterQuery() {
    if [ -z "$query" ]; then
        printf "NOTE: This version of SONARiS is DEPRECATED and will no longer be updated,\nplease use the POSIX version instead (./sonaris.sh).\n"
        printf "Search: "
        read -r query
    fi
}
enterQuery

# exit when necessary/asked to
if [ -z "$query" ]      || \
  [ "$query" = "q" ]    || [ "$query" = "Q" ]    || \
  [ "$query" = "wq" ]   || [ "$query" = "WQ" ]   || \
  [ "$query" = "exit" ] || [ "$query" = "quit" ] || \
  [ "$query" = ":q" ]   || [ "$query" = ":wq" ]; then
    echo "Goodbye!"
    exit
fi

# clean query
query=$(sed \
    -e 's|+|%2B|g'\
    -e 's|#|%23|g'\
    -e 's|&|%26|g'\
    -e 's| |+|g' <<< "$query")

# fetch info from results page
getResults() {
	results=$(curl "https://soundcloud.com/search?q=$query" 2>&1 /dev/null | \
        grep "$query" | \
        cut -c "24-" | \
        cut -d \" -f 1 | \
	    tail -n +4)
}
getResults

# let user choose and play/download what they asked for
chooseNPlay() {
    choice=$(echo "$results" | $finder)
    if [ -z "$choice" ]; then echo "Goodbye!"; exit; fi # if no choice then exit
    if [ "$player" = "youtube-dl" ]; then
        echo " "; echo -e "Downloaded song(s) will be placed in:${YELLOW} $downloadDir ${NC}"; echo " "
        $player "https://soundcloud.com$choice" -o "$downloadDir/$choice.mp3"
    else echo " "; echo -e "Press ${YELLOW}CTRL + C${NC} at any moment to abort and go back to the song picking screen."; echo " "
        $player "https://soundcloud.com$choice"
    fi
    chooseNPlay
}
chooseNPlay
